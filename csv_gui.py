#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from PySide import QtCore, QtGui

class Ui_csvScrap(object):
    def setupUi(self, csvScrap):
        csvScrap.setObjectName("csvScrap")
        csvScrap.resize(373, 167)
        csvScrap.setMinimumSize(QtCore.QSize(373, 167))
        csvScrap.setMaximumSize(QtCore.QSize(373, 167))
        self.line_src_folder = QtGui.QLineEdit(csvScrap)
        self.line_src_folder.setGeometry(QtCore.QRect(10, 30, 351, 24))
        self.line_src_folder.setObjectName("line_src_folder")
        self.line_dest_folder = QtGui.QLineEdit(csvScrap)
        self.line_dest_folder.setGeometry(QtCore.QRect(10, 80, 351, 24))
        self.line_dest_folder.setObjectName("line_dest_folder")
        self.line_search_pattern = QtGui.QLineEdit(csvScrap)
        self.line_search_pattern.setGeometry(QtCore.QRect(10, 130, 151, 24))
        self.line_search_pattern.setObjectName("line_search_pattern")
        self.label_3 = QtGui.QLabel(csvScrap)
        self.label_3.setGeometry(QtCore.QRect(11, 110, 98, 19))
        font = QtGui.QFont()
        font.setFamily("Serif")
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.actionGatherData = QtGui.QPushButton(csvScrap)
        self.actionGatherData.setGeometry(QtCore.QRect(277, 127, 85, 27))
        self.actionGatherData.setObjectName("actionGatherData")
        self.label_1 = QtGui.QLabel(csvScrap)
        self.label_1.setGeometry(QtCore.QRect(10, 10, 92, 19))
        font = QtGui.QFont()
        font.setFamily("Serif")
        font.setPointSize(10)
        self.label_1.setFont(font)
        self.label_1.setObjectName("label_1")
        self.label_2 = QtGui.QLabel(csvScrap)
        self.label_2.setGeometry(QtCore.QRect(10, 60, 122, 19))
        font = QtGui.QFont()
        font.setFamily("Serif")
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")

        self.retranslateUi(csvScrap)
        QtCore.QMetaObject.connectSlotsByName(csvScrap)

    def retranslateUi(self, csvScrap):
        csvScrap.setWindowTitle(QtGui.QApplication.translate("csvScrap", "csvScrap", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("csvScrap", "Search pattern", None, QtGui.QApplication.UnicodeUTF8))
        self.actionGatherData.setText(QtGui.QApplication.translate("csvScrap", "Gather Data", None, QtGui.QApplication.UnicodeUTF8))
        self.label_1.setText(QtGui.QApplication.translate("csvScrap", "Source folder", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("csvScrap", "Destination folder", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    csvScrap = QtGui.QWidget()
    ui = Ui_csvScrap()
    ui.setupUi(csvScrap)
    csvScrap.show()
    sys.exit(app.exec_())

