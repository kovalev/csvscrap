#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#-----------------------------------------------------------------------------
# Author:     Alexander Ray Kovalev
#
# Copyright:  (c) 2012 Alexander Kovalev
# E-mail:     ray.kovalev@gmail.com
# cvsScrap:   The user inputs a root directory and .csv file name. 
#             The script shall then crawl through the folder structure and 
#             gather data from .csv files that were specified by the user. 
#             The gathered data will be sorted and saved into a single .csv file.
#-----------------------------------------------------------------------------

import os
import sys
import glob
import csv
import csv_gui
from PySide import QtCore, QtGui
from operator import itemgetter, attrgetter

class csvScrap(QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = csv_gui.Ui_csvScrap()
        self.ui.setupUi(self)

        self.ui.actionGatherData.clicked.connect(self.actionGatherData)
    def actionGatherData(self):
        topdir = self.ui.line_src_folder.text()
        csv_data = []
        search_word = self.ui.line_search_pattern.text()
        dest_folder = self.ui.line_dest_folder.text()

        if(os.name == 'posix'): self.divider = '/'
        elif(os.name == 'nt'): self.divider = '\\'


        walklist = os.walk(topdir)
        for dirs in walklist:
            if(dirs[2]):
                fullpath = os.path.join(dirs[0], '*' + search_word + '*.csv')
                arr_files = glob.glob(fullpath)

                for cur_file in arr_files:
                    print 'Compiling data...'
                    hFile = open(cur_file, 'r')
                    for record in csv.reader(hFile):
                        date, author, isbn, price = record
                        stock = (date, str(author), int(isbn), int(price))
                        csv_data.append(stock)
                    hFile.close()

                    csv_data = sorted(csv_data, key=itemgetter(0))
                    csv_data = sorted(csv_data, key=itemgetter(3))
            else:
                pass
        try:
            save_data = 'all_' + search_word + '_.csv'
            dst_file = dest_folder + self.divider + save_data
            hFWrite = open(dst_file, 'w')
            writer = csv.writer(hFWrite)
            writer.writerows(csv_data)
            hFWrite.close()
            self.user_message('Data saved in ' + dst_file)
        except:
            self.user_massage('Can\'t write compiled data')

    def user_message(self, text):
        QtGui.QMessageBox.information(self, 'Information'.decode('UTF-8'), text.decode('UTF-8'), QtGui.QMessageBox.Ok)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    csvScrap = csvScrap()
    csvScrap.show()
    sys.exit(app.exec_())
